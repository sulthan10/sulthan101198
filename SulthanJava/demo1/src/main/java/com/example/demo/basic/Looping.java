package com.example.demo.basic;

import java.util.ArrayList;
import java.util.List;

public class Looping {
    public List<Integer> get(int limit){

        List<Integer> listPrime=new ArrayList<Integer>();
        boolean isPrime;

        for(int i=2;i<=limit;i++){

            isPrime=true;

            for(int j=2;j<i;j++){
                if(i%j==0){
                    isPrime=false;
                    break;
                }
            }

            if(isPrime)
                listPrime.add(i);
        }

        return listPrime;

    }
}