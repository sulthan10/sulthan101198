package com.example.demo.model;

import java.math.BigDecimal;

public class Mobil {
   private String nama ;
   private Integer jumlah ;
   private BigDecimal harga ;

    public Mobil(String nama){
        System.out.print(" Ini Adalah constructor mobil "+nama);
        this.nama=nama;
    }
    public Mobil(String nama,BigDecimal harga){
        System.out.print(" Ini Adalah constructor mobil "+nama+" Dan Harganya"+harga);
    }

    public Mobil(String nama,Integer jumlah,BigDecimal harga){
        System.out.print(" Ini Adalah constructor mobil "+nama+", Jumlah Mobil "+jumlah+", Dan Harganya "+harga);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }
}
